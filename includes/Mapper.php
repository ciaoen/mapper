<?php
/**
 * show many choise map
 */
class Mapper extends WP_Widget {
    public $widget_desc = "Choise many map";
    public $name = "Mapper choise";
    public $control_options = array('title'=>'Options','width' => 300,'height' => 200,'address'=>'Riga, Brivibas iela 69','type'=>1);

    function __construct() {
        $widget_options = array(
            'classname' => __CLASS__,
            'description' => $this->widget_desc,
        );
        parent::__construct(__CLASS__, $this->name, $widget_options, $this->control_options);
    }
    static function register_this_widget() {
        register_widget(__CLASS__);
    }
    
    /**
     * Load script
     */
     public static function scripts(){
       $style = plugins_url('css/geomap.css',  dirname(__FILE__));
       wp_register_style('dtw-mapper', $style);
       wp_enqueue_style('dtw-mapper');
       
       $type= get_option('typevalue',1);
       
       
       if($type == 1){
        wp_enqueue_script('gmap','http://maps.google.com/maps/api/js?sensor=false');
       $plaginMap = plugins_url('js/jquery.gomap-1.3.3.min.js',  dirname(__FILE__));
      
       }
       
       elseif ($type == 2) {
       wp_enqueue_script('bmap','http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0');
       $plaginMap = plugins_url('js/jquery.ui.bmap.min.js',  dirname(__FILE__));
   }
   
   elseif($type == 3){
       wp_enqueue_style('leafletjs',"http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css");
       wp_enqueue_script('leaft','http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js');
       wp_enqueue_script('mapopen','http://open.mapquestapi.com/sdk/leaflet/v1.s/mq-map.js?key=Fmjtd%7Cluu82q0y2l%2C85%3Do5-94ys0u');
       wp_enqueue_script('mapopen2','http://open.mapquestapi.com/sdk/leaflet/v1.s/mq-geocoding.js?key=Fmjtd%7Cluu82q0y2l%2C85%3Do5-94ys0u');

}
        
      wp_enqueue_script('jquery');
       $map = plugins_url('js/map.js',  dirname(__FILE__));
       wp_register_script('gomap', $plaginMap);
       wp_register_script('dtw-mapper', $map);
      
       
      
       wp_enqueue_script('gomap');
       wp_enqueue_script('dtw-mapper');
      
    }
    
    function widget($args, $instance) {
        $placeholders = array();
        $placeholders['address'] = get_option('addressvalue','address');
        $placeholders['type'] = get_option('typevalue','1');
        $placeholders['w'] = get_option('widthvalue');
        $placeholders['h'] = get_option('heightvalue');
        
        $tpl = file_get_contents(dirname(dirname(__FILE__)) . '/tpl/map.tpl');
        print Mapper::parse($tpl, $placeholders);
        
       
    }
    
     public function form($instance) {
        $placeholders = array();
        foreach ($this->control_options as $key => $val) {
            $placeholders[$key . '.id'] = $this->get_field_id($key);
            $placeholders[$key . '.name'] = $this->get_field_name($key);
               // This helps us avoid "Undefined index" notices.
            if (isset($instance[$key])) {
                $placeholders[$key . '.value'] = esc_attr($instance[$key]);
                update_option($key."value",esc_attr($instance[$key]));
            }
               // Use the default (for new instances)
            else {
               // update_option($key."value",$this->control_options[$key]);
                
                $placeholders[$key . '.value'] = $this->control_options[$key];
            }
        }
        $tpl = file_get_contents(dirname(dirname(__FILE__)) . '/tpl/form.tpl');
        print Mapper::parse($tpl, $placeholders);
      
    }
    
    /**
     * 
     * @param type $tpl
     * @param type $hash
     * @return template data
     */
     static function parse($tpl, $hash) {
        foreach ($hash as $key => $value) {
            $tpl = str_replace('{{' . $key . '}}', $value, $tpl);
        }
        return $tpl;
    }
    
    
   
    
}

?>
