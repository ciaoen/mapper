<?php
/**
 * Plugin Name: Mapper Choiser.
 * Plugin URI:  uder development
 * Description: build map
 * Version: 1.0
 * Author: dtwdveleop
 * Author URI: uder development
 * Text Domain: Optional. Plugin's text domain for localization. Example: mytextdomain
 * Domain Path: Optional. Plugin's relative directory path to .mo files. Example: /locale/
 * Network: Optional. Whether the plugin can only be activated network wide. Example: true
 * License: A short license name. Example: GPL2
 */
include_once 'includes/Mapper.php';
add_action('init', 'Mapper::scripts');
add_action('widgets_init', 'Mapper::register_this_widget');


?>